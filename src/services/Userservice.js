import axios from 'axios';

const API_BASE_URL = "http://localhost:8080/api/v1/";

class Userservice {

    getUsers(){
        return axios.get(API_BASE_URL + 'users');
    }

    getForms(){
        return axios.get(API_BASE_URL + 'form');
    }
    createuser(username){
        return axios.post(API_BASE_URL + 'signup');
    }

    getUserById(aadhar){
        return axios.get(API_BASE_URL + 'signin'+'/'+'id'+'/'+ aadhar);
    }

    getFormById(aadhar){
        return axios.get(API_BASE_URL + 'form'+'/'+'id'+'/'+ aadhar);
    }
    getUserByname(username){
        return axios.get(API_BASE_URL +'signin'+'/'+ 'username'+'/'+ username);
    }

    updateUser(user,aadhar){
         return axios.put(API_BASE_URL +'users'+'/'+ aadhar,user);
     }
    
     updateFormUser(user,aadhar){
        return axios.put(API_BASE_URL +'form'+'/'+ aadhar,user);
    }

    deleteUser(userid){
        return axios.delete(API_BASE_URL + 'users'+'/'+ userid);
    }

    deleteformuser(aadhar){
        return axios.delete(API_BASE_URL + 'form'+'/'+ aadhar);
    }
}

export default new Userservice()