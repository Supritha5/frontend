import React, { Component } from 'react'
import Userservice from '../services/Userservice'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import { Link } from "react-router-dom";
import { Table,Button } from 'react-bootstrap'; 
//import App from './App.js';


const loggedin = "true";
class ListuserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                users: [],
                forms:[],
                vname:this.props.match.params.username
                
                
        }
        
        this.deleteuser = this.deleteuser.bind(this);
        this.deleteformuser = this.deleteformuser.bind(this);
    }

    deleteuser(aadhar){
        Userservice.deleteUser(aadhar).then( res => {
            this.setState({users: this.state.users.filter(user => user.aadhar !== aadhar)});
        });
    }
    viewuser(aadhar){
        
        this.props.history.push(`/view/${aadhar}`);
    }

    
    verifyuser(aadhar){
        var reguser;
        
        Userservice.getUserByname(this.props.match.params.username).then((res=>{
            reguser=res.data;
        
        
        Userservice.getUserById(aadhar).then((res1=>{
            let u=res1.data;
            let user={status:"verified",verifiedby:reguser.fullname,approvedby:""}
            Userservice.updateUser(user,u.aadhar);
            alert("The details has been verified")
            window.location.reload();
        }))
    }));
    }

    deleteformuser(aadhar){
        
            Userservice.deleteformuser(aadhar).then( res => {
                this.setState({users: this.state.users.filter(user => user.aadhar !== aadhar)});
            });
            window.location.reload();
        }
        viewformuser(aadhar){
            this.props.history.push(`/view_form/${aadhar}`);
        }
        verifyformuser(aadhar){
            var reguser;
            Userservice.getUserByname(this.props.match.params.username).then((res=>{
                reguser=res.data;
            
            Userservice.getFormById(aadhar).then((res1=>{
                let u=res1.data;
                
                let user={status:"f_verified",verifiedby:reguser.fullname,approvedby:""}
                Userservice.updateFormUser(user,u.aadhar);
                alert("The details has been verified")
                window.location.reload();
            }))
        }));  
    }
    componentDidMount(){
        Userservice.getUsers().then((res) => {
            this.setState({ users: res.data});
        });

        Userservice.getForms().then((res1) => {
            this.setState({ forms: res1.data});
        });
        window.history.pushState(null, document.title, window.location.href);
        window.addEventListener('popstate', function (event)
        {   
            
            window.history.pushState(null, document.title, window.location.href);
           // alert("Please use Logout Button to quit")
        });
        
        // Userservice.getUserById(this.state.id).then((res=>{
        //     let u=res.data;
        //     u.status=this.state.status;
        //     this.setState({ status: this.state.status });
            
        //    // alert(u.this.state.id)
        // }))
        
    }
    render() {
        
        return (
            <div>  
               <div >
               <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top"  variant="dark">
                <Container>
                <Navbar.Brand href="#home">KBOCW</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav className="me-auto">
                    <Navbar.Brand href="#home">Verifier's Board</Navbar.Brand>
                   
                    </Nav>
                    <Nav>
                    <Navbar.Brand href="/">Logout</Navbar.Brand>
                    
                    </Nav>
                </Navbar.Collapse>
                </Container>
                </Navbar>
                    {/* <nav className="navbar navbar-expand-lg navbar-light fixed-top">
                    <div className="container">
                    <ul className="nav navbar-nav navbar-right" >
                           
                                <Link className="navbar-brand" to={"/logout"}><b>Logout</b></Link>
                                
                            
                    </ul>
                    
                    </div>
                    </nav> */}
                   
                </div>
                    <br/><br/>
                   
       
                     
                 <div className = "row">
                
                 <table className = "table table-striped table-bordered">
                 <thead>  
                    <tr className="table-active"><th colSpan="7"> <h5>User Details Verification</h5></th></tr>  
                </thead>  
                            <thead >
                                <tr>
                                   
                                    <th> User Fullname</th>
                                    <th> Address</th>
                                    <th> Contact Number</th>
                                    <th> Aadhar ID</th>
                                    <th> Gender</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                       {
                                        this.state.users.filter(user => user.status == "initiate").map(
                                        user => 
                                    
                                        <tr key = {user.id}> 
                                            
                                             <td> {user.fullname}</td>
                                             <td> {user.address}</td>
                                             <td> {user.contact}</td>
                                             <td> {user.aadhar}</td>
                                             <td> {user.gender}</td>
                                             <td>
                                                 
                                                 <button variant="info" style={{marginLeft: "10px"}} onClick={ () => this.deleteuser(user.aadhar)} className="btn btn-danger">Reject</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewuser(user.aadhar)} className="btn btn-info">View </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.verifyuser(user.aadhar)} className="btn btn-info">Verify </button>
                                             </td>
                                        </tr>
                                    )
                                        }
                            </tbody>
                            </table>  

                 </div>
                <br/>
                <br/>
                <br/>
                                        
                <div className = "row">
                 
                <table className = "table table-striped table-bordered">
                 <thead>  
                    <tr className="table-active"><th colSpan="7"><h5>User Application Form verification</h5></th></tr>  
                </thead> 
                            <thead >
                                <tr>
                                <th> Scheme Name</th>
                                    <th> User Fullname</th>
                                    <th> Address</th>
                                    <th> Contact Number</th>
                                    <th> Aadhar ID</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                       {
                                        this.state.forms.filter(form => form.status == "f_initiate").map(
                                        form => 
                                    
                                        <tr key = {form.id}>  
                                            <td> {form.schemename}</td>
                                             <td> {form.fullname}</td>
                                             <td> {form.address}</td>
                                             <td> {form.contact}</td>
                                             <td> {form.aadhar}</td>

                                             <td>
                                                 
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteformuser(form.aadhar)} className="btn btn-danger">Reject</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewformuser(form.aadhar)} className="btn btn-info">View </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.verifyformuser(form.aadhar)} className="btn btn-info">Verify </button>
                                             </td>
                                        </tr>
                                    )
                                        }
                            </tbody>
                        </table>

                 </div>
            </div>
        )
    }
}

export default ListuserComponent

 {/* //this.state.users.map(
                                       // user => */}