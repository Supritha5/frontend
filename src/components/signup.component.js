import React, { Component } from "react";
import axios from 'axios';
import Userservice from "../services/Userservice";
//import { Col, Row, Form } from "react-bootstrap";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
//import regexValidators from './regex-validators';
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'

const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};


var uname;
const vusername = value => {
  var n1=value;
  Userservice.getUserByname(n1).then((res=>{
    let u1=res.data
    uname=u1.username;
    
}))
  
if(uname == value){
  return (  
    <div className="alert alert-danger" role="alert">
      Username already taken ,Please try others, include @,#,$,%,_ in username for it to be unique
    </div>
  );
}
 else if (value.length < 6 || value.length > 20 ) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 6 and 20 characters.
      </div>
    );
  }
};



var val1;
const vpassword = value => {
  val1=value;


  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        The password must be between 6 and 40 characters.
      </div>
    );
  }
};

const vpassword2 = value => {
  if (value !=val1) {
   
    return (
      <div className="alert alert-danger" role="alert">
        Passwords are not matching
      </div>
    );
  }
};

var aad;
const vaadhar = value => {
  //aaad;
  var d=value;
  Userservice.getUserById(d).then((res=>{
    let u=res.data
    aad=u.aadhar;
    
}))


    if(aad == value){
      return (  
        <div className="alert alert-danger" role="alert">
          User Already Exists Please Login
        </div>
      );
    }

  else if (value.length < 12 || value.length > 12) {
    return (
              
            <div className="alert alert-danger" role="alert">
               Please enter valid Aadhar Number 
               </div>
             );
    
        }
        
  };
        
        


const vcontact = value => {
  if (value.length < 10 || value.length > 10) {
   
    return (
      <div className="alert alert-danger" role="alert">
        Enter valid Phone Number (10 digits)
      </div>
    );
  }
};

export default class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
          user:{},
          users:[],
          fullname:'',
          username: '',
          password: '',
          contact: '',
            aadhar:'',
	          address:'',
            status: '',
           
        };
        this.myChangeHandler = this.myChangeHandler.bind(this);
        //this.check=this.check.bind(this);
      }
      myChangeHandler(event){
          this.setState({
            // Computed property names
            // keys of the objects are computed dynamically
            [event.target.name] : event.target.value
          })
        }
      componentDidMount(){//method is called after the component is rendered.
          axios.get("http://localhost:8080/api/v1/users")
          .then((res)=>{
            this.setState({
            users:res.data,
            fullname:'',
            username: '',
            password: '',
            contact: '',
            aadhar:'',
	          address:'',
            gender:'',
            status: ''
            })
          });
        }
     
        
        submit(event,id)
        { 

          event.preventDefault();
         
          alert(`
          Details has been succesfully sent for verification and approval
          `)
         
            axios.post("http://localhost:8080/api/v1/signup",{
              fullname:this.state.fullname,
              username: this.state.username,
              password: this.state.password,
              contact: this.state.contact,
                aadhar: this.state.aadhar,
                address: this.state.address,
                gender:this.state.gender,
              status: 'initiate'
            })
            .then((res)=>{
              this.componentDidMount();
            })
            //this.formRef.reset();
           // event.target.reset()
            window.location.reload()
          }

    render() {
        return (
            <div class ="b">
            <Form class="sup" 
            onSubmit={(e)=>this.submit(e)}
            ref={(ref)=>this.formRef=ref} >
            {/* ref={(ref)=>this.formRef=ref} onSubmit={(e)=>this.submit(e)} > */}
              <h3>Registration Form</h3>

              <div class="col-md-12">
                   
                      <div>
                      <div className="form-group">
                          <label for="validationServer02" class="form-label">Full name</label>
                              <Input type="text" className="form-control" name="fullname"  onChange={this.myChangeHandler} 
                              validations={[required]} placeholder="Enter User Full name as per the Aadhar" required />
                              
                          </div>
                          <div className="form-group">
                          <label for="validationServer02" class="form-label">Username</label>
                              <Input type="text" className="form-control" name="username" required onChange={this.myChangeHandler} 
                              validations={[required, vusername]} placeholder="Enter Username(unique) " />
                              
                          </div>
                          <div className="form-group">
                              <label>Create Password</label>
                              <Input type="password" className="form-control" name="password" required onChange={this.myChangeHandler} placeholder="Enter password" 
                              validations={[required, vpassword]} />
                          
                          </div>
                          <div className="form-group">
                              <label>Confirm Password</label>
                              <Input type="password" className="form-control" name="password" required onChange={this.myChangeHandler} placeholder="Enter password" 
                              validations={[required, vpassword2]} />
                          
                          </div>
                          <div className="form-group">
                              <label>Address</label>
                              <Input type="text" className="form-control" name="address" required onChange={this.myChangeHandler} placeholder="Enter address" />
                          </div>
                          <div className="form-group">
                              <label>Contact Number</label>
                              <Input type="numeric" className="form-control" name="contact" required onChange={this.myChangeHandler} placeholder="Enter contact number"  validations={[required, vcontact]}/>
                          </div>
                          <div className="form-group">
                              <label>Aadhar Number</label>
                              <Input type="numeric" className="form-control" name="aadhar" required onChange={this.myChangeHandler} placeholder="Enter Aadhar ID"  validations={[required, vaadhar]}/>
                          </div>
                          <div class="form-group col-md-6">
                            <label>Gender </label><br />
                            <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="gender" id="inlineRadiom" value="male"  onChange={this.myChangeHandler} validations={[required]} required/>
                                        <label class="form-check-label" for="inlineRadiom">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="gender" id="inlineRadiof" value="female"  onChange={this.myChangeHandler} validations={[required]} required />
                                        <label class="form-check-label" for="inlineRadiof">Female</label>
                            </div>
                          </div>

                          {/* <div className="form-group"> */}
                              <button type="submit" className="btn btn-primary btn-block"  name="action">Register</button>
                              <p className="forgot-password text-right">
                                  Already registered <a href="/sign-in">sign in?</a>
                              </p>
                          {/* </div> */}
                      </div>
                  
                </div>
               

            
            </Form>
            </div>
        );
    }
}