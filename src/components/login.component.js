import React, { Component } from "react";
import Userservice from "../services/Userservice";


export const loggedin = "true";
export default class Login extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            id: this.props.match.params.id,
            username:'',
            password:'',
            status1:''
        }
        this.myChangeHandler = this.myChangeHandler.bind(this);
        
    }

    componentDidMount(){
        
        window.history.pushState(null, document.title, window.location.href);
        window.addEventListener('popstate', function (event)
        {
            window.history.pushState(null, document.title, window.location.href);
        });
    }

    handleLogin = (e) => {
        e.preventDefault();
       
        let user = {username: this.state.username,password:this.state.password};
       
        Userservice.getUserByname(user.username).then((res=>{
            let u=res.data;
           
            if(u.status=="ver") 
            {   if (u.password==user.password){
                    
                    this.props.history.push(`/verify/${u.username}`)
                }
                else{
                    
                    alert("Please verify your username and password");
                    window.location.reload();
                   
                }
            }
            else if(u.status=="app"){
                if (u.password==user.password){
                    this.props.history.push(`/approve/${u.username}`);
                }
                else{
                    alert("Please verify your username and password");
                    window.location.reload();
                }
                
            }
            else if (u.status=="approved"){
                if (u.password==user.password){
                    this.props.history.push("/form/"+u.aadhar);
                    
                }
                else{
                    alert("Please verify your username and password");
                    window.location.reload()
                }
            }
            else if(u.status=="initiate"){
                alert("Your details are yet to be verified");
                window.location.reload()
            }
            else if(u.status=="verified"){
                
                alert("Your details are yet to be approved");
                window.location.reload()
            }
            else{
               alert("Please enter valid username and password")
                window.location.reload()
            }

        }))
    }
    
    myChangeHandler= (event) => {
        this.setState({
            // Computed property names
            // keys of the objects are computed dynamically
            [event.target.name] : event.target.value
          })
    }

    

    render() {
        return (
            <div class="b">
            <form class="sup" >
                <h3>Sign In</h3>
                <div class="col-md-12">
                <div className="form-group">
                    <label>Username</label>
                    <input type="username" className="form-control"  name="username" onChange={this.myChangeHandler} placeholder="Enter username" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" onChange={this.myChangeHandler} placeholder="Enter password" />
                </div>
{/*                 
                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div> */}
                
               
                <button type="submit" className="btn btn-primary btn-block" onClick={this.handleLogin}>Login</button>
                </div>
                {/* <p className="forgot-password text-right">
                    Forgot <a href="#">password?</a>
                </p> */}
            </form>
            </div>
        );
    }
}