import React, { Component } from 'react'
import Userservice from '../services/Userservice'
import { Button, Container, Form, FormGroup, Input, Label, NavItem } from 'reactstrap';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Navbar from 'react-bootstrap/Navbar'
import { withRouter } from 'react-router-dom';
//import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'

import { Table} from 'react-bootstrap';

class Viewforapprover extends Component {
    constructor(props) {
        super(props)

        this.state = {
            aadhar:this.props.match.params.aadhar,
            user: {},
            username:'',
            contact: '',
	        address:'',
            gender:'',
        }
    }

    componentDidMount(){
        Userservice.getUserById(this.state.aadhar).then( res => {
            this.setState({user: res.data});
        })
       // alert(this.state.aadhar)
    }

    render() {
        return (
            <div>
                <div >
                <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top"  variant="dark">
                <Container>
                <Navbar.Brand href="#home">KBOCW</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav className="me-auto">
                    <Navbar.Brand href="#home">Approver's Board</Navbar.Brand>
                   
                    </Nav>
                    <Nav>
                    <Navbar.Brand href="/logout">Logout</Navbar.Brand>
                    
                    </Nav>
                </Navbar.Collapse>
                </Container>
                </Navbar>
                   
                </div>
                <br/><br/><br/>
            <div className = "card col-md-6 offset-md-3">
                   
         <div className = "card-body">
          
            <Table className="table">  
            <thead>  
              <tr className="btn-primary"><th colSpan="2">User Details</th></tr>  
            </thead>  
            <tbody>  
  
              <tr>
               
                <th>Username </th><td>{this.state.user.username}</td>  
              </tr> <tr>  
                <th>Contact </th><td>{this.state.user.contact}</td>  
              </tr> <tr>  
                <th>Address  </th><td>{this.state.user.address}</td>  
              </tr> <tr>  
                <th>Aadhar ID  </th><td>{this.state.user.aadhar}</td>  
               </tr>
              <tr>  
                <th>Gender  </th><td>{this.state.user.gender}</td>  
              </tr> 
              <tr>  
                <th>Verifier's Name  </th><td>{this.state.user.verifiedby}</td>  
              </tr>  
               
              
            </tbody>  
          </Table>  
                          
                    <FormGroup>
                    <Button color="secondary" onClick={this.props.history.goBack}>Back</Button>
                        {/* <Button color="secondary" tag={Link} to="/approve/">Back</Button> */}
                </FormGroup>
                </div>
            </div>
            </div>
        )
    }
}

export default withRouter(Viewforapprover);