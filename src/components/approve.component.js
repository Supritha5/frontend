import React, { Component } from 'react'
import Userservice from '../services/Userservice'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
class ApproveuserComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                users: [],
                forms:[],
                vname:this.props.match.params.username
                
        }
        
        this.deleteuser = this.deleteuser.bind(this);
        this.deleteformuser = this.deleteformuser.bind(this);
    }

    deleteuser(aadhar){
        Userservice.deleteUser(aadhar).then( res => {
            this.setState({users: this.state.users.filter(user => user.aadhar !== aadhar)});
        });
    }
    viewuser(aadhar){
        this.props.history.push(`/view_a/${aadhar}`);
    }
    
    approveuser(aadhar){
        var reguser;
        Userservice.getUserByname(this.props.match.params.username).then((res=>{
            reguser=res.data;
       
        Userservice.getUserById(aadhar).then((res1=>{
                let u=res1.data;
                let user={status:"approved",verifiedby:u.verifiedby,approvedby:reguser.fullname} 
                Userservice.updateUser(user,u.aadhar);
                alert("The user details has been Approved")
                window.location.reload();
            }))
            
        }));
            
  
    }
    deleteformuser(aadhar){
        Userservice.deleteformuser(aadhar).then( res => {
            this.setState({users: this.state.users.filter(user => user.aadhar !== aadhar)});
        });
        window.location.reload();
    }
    viewformuser(aadhar){
        this.props.history.push(`/view_fa/${aadhar}`);
    }
    approveformuser(aadhar){
        var reguser;
        Userservice.getUserByname(this.props.match.params.username).then((res=>{
            reguser=res.data;
        
        Userservice.getFormById(aadhar).then((res1=>{
            let u=res1.data;
            
            let user={status:"f_approved",verifiedby:u.verifiedby,approvedby:reguser.fullname}
            Userservice.updateFormUser(user,u.aadhar);
            alert("From has been approved")
            window.location.reload();
        }))   
    }));
}
    componentDidMount(){
        Userservice.getUsers().then((res) => {
            this.setState({ users: res.data});
        });

        Userservice.getForms().then((res1) => {
            this.setState({ forms: res1.data});
        });
        window.history.pushState(null, document.title, window.location.href);
        window.addEventListener('popstate', function (event)
        {   
            
            window.history.pushState(null, document.title, window.location.href);
            //alert("Please use Logout Button to quit")
        });
    }
    render() {
        //const loggingIn  = "true";
        return (
            <div>
                <div >
                <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top"  variant="dark">
                <Container>
                <Navbar.Brand href="#home">KBOCW</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav className="me-auto">
                    <Navbar.Brand href="#home">Approver's Board</Navbar.Brand>
                   
                    </Nav>
                    <Nav>
                    {/* <Navbar.Brand href="/logout">Logout</Navbar.Brand> */}
                    <Link className="navbar-brand" to={"/"}>Logout</Link>
                    </Nav>
                </Navbar.Collapse>
                </Container>
                </Navbar>
                    
                </div>
                 <br/>
                 <br/>
                 <br/>
                 <div className = "row">
                    
                        <table className = "table table-striped table-bordered">
                        <thead>  
                            <tr className="table-active"><th colSpan="7"> <h5>User Details approval</h5></th></tr>  
                        </thead>
                            <thead>
                                <tr>
                                    
                                    <th> User Fullname</th>
                                    <th> Address</th>
                                    <th> Contact Number</th>
                                    <th> Aadhar ID</th>
                                    <th> Gender</th>
                                    <th> Verified By</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                      
                                   
                                       {
                                        this.state.users.filter(user => user.status == "verified").map(
                                        user => 
                                    
                                        <tr key = {user.id}>  
                                        
                                            <td> {user.fullname}</td>
                                             <td> {user.address}</td>
                                             <td> {user.contact}</td>
                                             <td> {user.aadhar}</td>
                                             <td> {user.gender}</td>
                                             <td> {user.verifiedby}</td>
                                             <td>
                                                 
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteuser(user.aadhar)} className="btn btn-danger">Reject</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewuser(user.aadhar)} className="btn btn-info">View</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.approveuser(user.aadhar)} className="btn btn-info">Approve</button>
                                             </td>
                                        </tr>
                                    )
                                        }
                            </tbody>
                        </table>

                 </div>
                 <br/>
                 <br/>
                 <br/>
                 <div className = "row">
                     
                        <table className = "table table-striped table-bordered">
                        <thead>  
                            <tr className="table-active"><th colSpan="7"><h5>User Application Form approval</h5></th></tr>  
                        </thead>
                            <thead>
                                <tr>
                                    <th>Scheme Name</th>
                                    <th> User Fullname</th>
                                    <th> Address</th>
                                    <th> Contact Number</th>
                                    <th> Aadhar ID</th>
                                    <th> Verified By</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                      
                                   
                                       {
                                        this.state.forms.filter(form => form.status == "f_verified").map(
                                        form => 
                                    
                                        <tr key = {form.id}>  
                                            <td>{form.schemename}</td>
                                            <td> {form.fullname}</td>
                                             <td> {form.address}</td>
                                             <td> {form.contact}</td>
                                             <td> {form.aadhar}</td>
                                             <td> {form.verifiedby}</td>
                                             <td>
                                                 
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteformuser(form.aadhar)} className="btn btn-danger">Reject</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewformuser(form.aadhar)} className="btn btn-info">View</button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.approveformuser(form.aadhar)} className="btn btn-info">Approve</button>
                                             </td>
                                        </tr>
                                    )
                                        }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ApproveuserComponent

 {/* //this.state.users.map(
                                       // user => */}