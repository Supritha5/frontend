import React, { Component } from "react";
import Userservice from '../services/Userservice'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {Sidebar} from "./sidebar.component";
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
export default class Form extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            aadhar: this.props.match.params.aadhar,
            username:'',
            password:'',
            status1:''
        }
        //this.myChangeHandler = this.myChangeHandler.bind(this);
        
    }

    componentDidMount(){
        Userservice.getUserById(this.state.aadhar).then( (res) =>{
            let user = res.data;
            
            this.setState({username: user.username,
                password: user.password,
                status : user.status
            });
            
        });
        window.history.pushState(null, document.title, window.location.href);
        window.addEventListener('popstate', function (event)
        {   
            
            window.history.pushState(null, document.title, window.location.href);
            alert("Please use Logout Button to quit")
        });
        
    }
    render() {
        //const loggingIn  = "true";
        return (
            <div class="container4">
                <div >
                <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top"  variant="dark">
                <Container>
                <Navbar.Brand href="#home">KBOCW</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav className="me-auto">
                    <Navbar.Brand href="#home">Benifit Form</Navbar.Brand>
                   
                    </Nav>
                    <Nav>
                    <Navbar.Brand href="/">Logout</Navbar.Brand>
                    
                    </Nav>
                </Navbar.Collapse>
                </Container>
                </Navbar>
                
                </div>
                <div> <h3  className="text-center">Welcome User</h3></div>
               
                <Sidebar width={300} height={"100vh"} >
                    <div className="scheme">
                        <br/>
                    <Link to={"/scheme/"+ this.state.aadhar} ><h4 className="text-center">Education Assistance Scheme</h4></Link>
                    <Link to="/" ><h4 className="text-center">Under Construction</h4></Link>
                    <Link to="/"><h4 className="text-center">Under Construction</h4></Link>
                    <Link to="/" ><h4 className="text-center">Under Construction</h4></Link>
                    <Link to="/" ><h4 className="text-center">Under Construction</h4></Link>
                    <Link to="/" ><h4 className="text-center">Under Construction</h4></Link>
                    <Link to="/" ><h4 className="text-center">Under Construction</h4></Link>
                    <Link to="/" ><h4 className="text-center">Under Construction</h4></Link>
                    <Link to="/" ><h4 className="text-center">Under Construction</h4></Link>
                    

                    </div>
                </Sidebar> 
                        

        </div>
        );
    }
}