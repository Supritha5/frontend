import React, { Component } from 'react'
import Userservice from '../services/Userservice'
import { Button, Container, Form, FormGroup, Input, Label, NavItem } from 'reactstrap';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Table} from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar'
import { withRouter } from 'react-router-dom';
//import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
class Viewformverifier extends Component {
    constructor(props) {
        super(props)

        this.state = {
            aadhar:this.props.match.params.aadhar,
            user: {},
            username:'',
            contact: '',
	        address:'',
            gender:'',
        }
    }

    componentDidMount(){
        Userservice.getFormById(this.state.aadhar).then( res => {
            this.setState({user: res.data});
        })
       // alert(this.state.aadhar)
    }

    render() {
        return (
            <div>
               <div >
               <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top"  variant="dark">
                <Container>
                <Navbar.Brand href="#home">KBOCW</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav className="me-auto">
                    <Navbar.Brand href="#home">Verifier's Board</Navbar.Brand>
                   
                    </Nav>
                    <Nav>
                    <Navbar.Brand href="/logout">Logout</Navbar.Brand>
                    
                    </Nav>
                </Navbar.Collapse>
                </Container>
                </Navbar>
                   
                </div>
                <br/><br/><br/>
            <div className = "card col-md-6 offset-md-3">
                   
         <div className = "card-body">
          
            <Table className="table">  
            <thead>  
              <tr className="btn-primary"><th colSpan="2">User Details</th></tr>  
            </thead>  
            <tbody>  
              
              <tr>  
                <th>Scheme name </th><td>{this.state.user.schemename}</td>  
              </tr>
              <tr>  
                <th>Username </th><td>{this.state.user.username}</td>  
              </tr> <tr>  
                <th>Contact </th><td>{this.state.user.contact}</td>  
              </tr> <tr>  
                <th>Address  </th><td>{this.state.user.address}</td>  
              </tr> <tr>  
                <th>Aadhar ID  </th><td>{this.state.user.aadhar}</td>  
              </tr> <tr>  
                <th>Ration Card Number  </th><td>{this.state.user.ration}</td>  
              </tr> 
              <tr>  
                <th>Gender  </th><td>{this.state.user.gender}</td>  
              </tr>  
              <tr>  
                <th>Date of Birth  </th><td>{this.state.user.dob}</td>  
              </tr>
              <tr>  
                <th>State  </th><td>{this.state.user.state}</td>  
              </tr>  
              <tr>  
                <th>City  </th><td>{this.state.user.city}</td>  
              </tr>
              <tr>  
                <th>Taluk  </th><td>{this.state.user.taluk}</td>  
              </tr>
              <tr>  
                <th>Village  </th><td>{this.state.user.village}</td>  
              </tr>
              <tr>  
                <th>Grama Panchayath  </th><td>{this.state.user.gram}</td>  
              </tr>
              
              <tr>  
                <th>Registration number  </th><td>{this.state.user.regno}</td>  
              </tr>
              <tr>  
                <th>Age at registration  </th><td>{this.state.user.ageatreg}</td>  
              </tr>
              <tr>  
                <th>Data of Registration  </th><td>{this.state.user.dateofreg}</td>  
              </tr>  
              <tr>  
                <th>Bankname  </th><td>{this.state.user.bankname}</td>  
              </tr>
              <tr>  
                <th>Bank Account Number  </th><td>{this.state.user.bankaccnumber}</td>  
              </tr>
              <tr>  
                <th>Bank Address  </th><td>{this.state.user.bankaddr}</td>  
              </tr>
              <tr>  
                <th>Bank Branch  </th><td>{this.state.user.bankbranch}</td>  
              </tr>
              <tr>  
                <th>Bank IFSC Code  </th><td>{this.state.user.bankifsc}</td>  
              </tr>
              
            </tbody>  
          </Table>  
                          
                    <FormGroup>
                      <Button color="secondary" onClick={this.props.history.goBack}>Back</Button>
                        {/* <Button color="secondary" tag={Link} to="/verify">Back</Button> */}
                </FormGroup>
                </div>
            </div>
            </div>
        )
    }
}

export default withRouter(Viewformverifier);