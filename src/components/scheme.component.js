import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import axios from 'axios';
import waitFor from '@testing-library/react';
import Navbar from 'react-bootstrap/Navbar'
//import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
//import Form from 'react-bootstrap/Form';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import { Col, Row,  FormGroup,FormControl,Checkbox} from "react-bootstrap";

import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import FloatingLabel from "react-bootstrap-floating-label";
import { wait } from '@testing-library/react';


const required = value => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const vdob = value => {
  var d1 = value.split('-');
  //var d2 = value.split('-');
  var yy=d1[0]
  var mm=d1[1]
  var dd=d1[2]
  //alert(yy)
  
  if (yy<1950 || yy>=2021) {
             return (
               <div className="alert alert-danger" role="alert">
               Please enter valid Date of birth
               </div>
             );
    
         }     
  };

const vration = value => {
    
    if (value.length<10 || value.length>10) {
               return (
                 <div className="alert alert-danger" role="alert">
                 Please enter valid ration card number
                 </div>
               );
      
           }     
    };

    const vregno = value => {
    
      if (value.length<10 || value.length>10) {
                 return (
                   <div className="alert alert-danger" role="alert">
                   Please enter valid Registration Number
                   </div>
                 );
        
             }     
      };      
      
class Scheme extends Component {
    emptyItem = {
        schemename:'Educational Assistance',
         username: '',
          contact: '',
          address:'',
          status: '',
          taluk:'',
          dob:'',
          city:'',
          state:'',
          village:'',
          ration:'',
          gram:'',
          regno:'',
          ageatreg:'',
          dateofreg:'',
          bankname:'',
          bankaccnumber:'',
          bankifsc:'',
          bankaddr:'',
          bankbranch:'',
  };
constructor(props){
    super(props);
   
    this.state ={
        country: "India",
        state: '',
        item: this.emptyItem,
  };
    
  this.handleChange = this.handleChange.bind(this);
 
}

async componentDidMount() {
    if (this.props.match.params.aadhar !== 'new') {
      const group = await (await fetch(`http://localhost:8080/api/v1/signin/id/${this.props.match.params.aadhar}`)).json();
      this.setState({item: group});
      console.log(group.username)
      
    }
  }
  handleChange(event) {
    this.setState({
        
        [event.target.name] : event.target.value
      })
  }
  selectCountry (val) {
    this.setState({ country: 'India' });
  }
  selectRegion (val) {
    this.setState({ state: val });
  }
  

        submit(event,id)
        {
          event.preventDefault();
          alert("Please verify your details before submission.....")
          const {item} = this.state;
          
         
          
            axios.post("http://localhost:8080/api/v1/submit",{
              schemename:'Educational Assistance',
              username:item.username,
              fullname:item.fullname,
              contact: item.contact,
                aadhar: item.aadhar,
                address: item.address,
                gender:item.gender,
              status: 'f_initiate', 
                taluk: this.state.taluk,
                dob:this.state.dob,
                city: this.state.city,
                state:this.state.state,
                village: this.state.village,
                ration: this.state.ration,
                gram: this.state.gram,
                regno: this.state.regno,
                ageatreg:this.state.ageatreg,
                dateofreg: this.state.dateofreg,
                bankname: this.state.bankname,
                bankaccnumber: this.state.bankaccnumber,
                bankifsc: this.state.bankifsc,
                bankaddr: this.state.bankaddr,
                bankbranch: this.state.bankbranch
            })
            .then((res)=>{
              this.componentDidMount();
            })
            alert(`
            Details has been succesfully sent for verification and approval
            `)
            
            //this.formRef.reset();
            this.props.history.push('/form/'+item.aadhar);
            window.location.reload()
        }


  render() {
    const {item} = this.state;
    const { country, state } = this.state;
    return (
    <div>
        <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top"  variant="dark">
                <Container>
                <Navbar.Brand href="#home">KBOCW</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav className="me-auto">
                    <Navbar.Brand href="#home">Application for Educational Assistance</Navbar.Brand>
                   
                    </Nav>
                    <Nav>
                    <Link className="navbar-brand" to={"/form/"+ this.props.match.params.aadhar}>Back</Link>
                    {/* //< Navbar.Brand Link to={"/form/"+ this.props.match.params.aadhar}>Back</Navbar.Brand> */}
                    {/* <Navbar.Brand href="/logout/">Back</Navbar.Brand>  */}
                    
                    </Nav>
                </Navbar.Collapse>
                </Container>
                </Navbar>
  <br/><br/>
    <Container>
        
      <Form className="scheme" ref={(ref)=>this.formRef=ref} onSubmit={(e)=>this.submit(e)}><br />
      <h6>Please fill in the required details</h6>    
        <div className="form-group">
            <Row className="g-2">
            <Col md>
            <label>Name</label>
            <Input type="username" className="form-control" name="fullname"   value={item.fullname } defaultValue={item.username}
                        onChange={this.handleChange} readOnly="true"  autoComplete="name" />
            <br/>   
            <label>Contact Number</label>
            <Input type="numeric" className="form-control" name="contact" value={item.contact } defaultValue={item.contact}
                        onChange={this.handleChange} readOnly="true"   />
            <br/>
            <label>Data of Birth</label>
            <Input type="date" className="form-control" name="dob" validations={[required, vdob]} onChange={this.handleChange}  required />
            <br/> 
            <div class="form-group col-md-6">
                <label>Gender </label><br />
                <Input type="text" className="form-control" name="gender" value={item.gender } defaultValue={item.gender}
                        onChange={this.handleChange} readOnly="true"   />
                
             </div>
             <br/>
            <h5>Address Details</h5>  
            <label>State </label>
            <RegionDropdown className="form-control" country={country} value={state} validations={[required]} name="state" onChange={(val) => this.selectRegion(val)} />
          
            <br/>
            <label>Taluk </label>
                 <Input type="text" className="form-control" name="taluk" onChange={this.handleChange} validations={[required]} placeholder="Enter Taluk" />
            <br/>
            <label>Village/Ward </label>
            <Input type="text" className="form-control" name="village" onChange={this.handleChange} validations={[required]} placeholder="Enter Village/Ward" />
            <br/>
        </Col>
        <Col md>
        <div className="form-group">
            <label>Aadhar</label>
            <Input type="numeric" className="form-control" name="aadhar" value={item.aadhar } defaultValue={item.aadhar}
                        onChange={this.handleChange} readOnly="true"  />
            <br/> 
            <label>Address</label>
            <Input type="text" className="form-control" name="address" value={item.address } defaultValue={item.address}
                        onChange={this.handleChange} readOnly="true" />
            <br/>
            <label>Ration Card </label>
            <Input type="numeric" className="form-control" name="ration" required validations={[required, vration]} onChange={this.handleChange} placeholder="Enter Ration card Number" />
            <br/>  
            <br/>
            <br/>
            <br/>
           <h5></h5> 
           <br/>
           <h5></h5> 
           <h5></h5> 
            <label>City </label>
            <Input type="text" className="form-control" name="city" required onChange={this.handleChange} validations={[required]} placeholder="Enter City" />
            <br/>
            <label>Gram Panchayat </label>
            <Input type="text" className="form-control" name="gram" required onChange={this.handleChange}  validations={[required]} placeholder="Enter Grama Panchayat" />
            <br/>
            
        </div>
        </Col>
        </Row>
        <div>
        <h5>Registration Details</h5>
        <Row className="g-2">
        <Col md>
        <label>Registartion Number</label>
            <Input type="numeric" className="form-control" name="regno" required onChange={this.handleChange} validations={[required, vregno]} placeholder="Enter Registartion Number" />
            <br/>
        <label>Age at the time of Registartion</label>
            <Input type="text" className="form-control" name="ageatreg" required onChange={this.handleChange} validations={[required]} placeholder="Enter age at the time of Registartion" />
            <br/>
        </Col>
        <Col>
        <label>Date of Registartion</label>
            <Input type="date" className="form-control" name="dateofreg" required onChange={this.handleChange} validations={[required]} placeholder="Enter Date of Registartion" />
            <br/>
        </Col>
        </Row>
        </div>
        <div>
        <h5>Bank Details</h5>
        <Row className="g-2">
        <Col md>
        <label>Bank Name</label>
            <Input type="name" className="form-control" name="bankname" onChange={this.handleChange} required validations={[required]} placeholder="Enter Bank Name" />
            <br/>
        <label>Bank Account Number</label>
            <Input type="numeric" className="form-control" name="bankaccnumber" onChange={this.handleChange}required validations={[required]} placeholder="Enter Bank Account Number" />
            <br/>
        <label>IFSC Code</label>
            <Input type="numeric" className="form-control" name="bankifsc" onChange={this.handleChange}required validations={[required]} placeholder="Enter IFSC Code" />
            <br/>
        </Col>
        <Col>
        <label>Branch Name</label>
            <Input type="name" className="form-control" name="bankbranch" onChange={this.handleChange} required validations={[required]} placeholder="Enter Branch Name" />
            <br/>
        <label>Bank Address</label>
            <Input type="text" className="form-control" name="bankaddr" onChange={this.handleChange}  required validations={[required]} placeholder="Enter Bank Address" />
            <br/>
        </Col>
        </Row>
        </div>
        <div >
         
          <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" name="check" id="inlineCheckboxh1" value="1" onChange={this.handleChange} required validations={[required]}/>
          <label class="form-check-label" for="inlineCheckboxh1">I here by  declare that the particulars furnished in this form are true to the best of my knowledge </label>
         </div>
        
        </div>
        <div class="form-row">
          <br/>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary" name="action">Submit</button>
             </div>
       </div>
        </div>
      </Form>
    </Container>
    </div>
    );
  }
       
}

export default Scheme;