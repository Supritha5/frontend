import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Login from "./components/login.component";
import SignUp from "./components/signup.component";
import Home from "./components/home.component";
import ListuserComponent from "./components/verify.component";
import ViewuserComponent from "./components/view.component";
import Viewforapprover from "./components/view_approver";
import ApproveuserComponent from "./components/approve.component";
import Form from "./components/form.component";
import Scheme from "./components/scheme.component";
import loggedin from "./components/verify.component";
import ViewFormApprover from "./components/view_form_approver.component";
import Viewformverifier from "./components/view_form.component";
function App() {
 // const  = "false";
 
  return (
  <Router>
    <div className="App">
      <Navbar collapseOnSelect expand="lg" bg="dark" fixed="top"  variant="dark">
                <Container>
                <Navbar.Brand href="/">KBOCW</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav className="me-auto">
                    <Navbar.Brand href="#home"></Navbar.Brand>
                    </Nav>
                    <Nav>
                    <Link className="navbar-brand" to={"/sign-up"}>Register</Link>
                    <Link className="navbar-brand" to={"/sign-in"}>Login</Link>
                    
                    </Nav>
                </Navbar.Collapse>
                </Container>
                </Navbar>

      <div className="auth-wrapper">
        <div className="auth-inner">
          <Switch>
            <Route exact path='/'  component={Home} />
            <Route path="/sign-in" component={Login} />
            <Route path="/sign-up" component={SignUp} />
            <Route path="/verify/:username" component={ListuserComponent} />
            <Route path="/approve/:username" component={ApproveuserComponent} />
            <Route path="/view/:aadhar" component={ViewuserComponent}/> 
            <Route path="/view_a/:aadhar" component={Viewforapprover}/> 
            <Route path="/form/:aadhar" component={Form} />
            <Route path="/form" component={Form} />
            <Route path="/logout" component={Home} />
            
            <Route path="/scheme/:aadhar" component={Scheme} />
            <Route path="/view_fa/:aadhar" component={ViewFormApprover} />
            <Route path="/view_form/:aadhar" component={Viewformverifier} />
            
          </Switch>
        </div>
      </div>
    </div>
    </Router>
  );
}

export default App;